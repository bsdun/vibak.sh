# vibak.sh

# Backup script for vi

**Create a backup directory**

mkdir ~/.vibackup

**Copy script into your prefered directory (or home directory)**

cp ~/Downloads/vibak.sh ~/

**Make script executable**

chmod +x ~/vibak.sh

**Make alias for the script**

alias vib='sh ~/vibak.sh'
