#!bin/sh

# Backup script for vi
# License: ISC
# Maintainer: Steve
# Last Change: 2020 Feb 25

viBackup="$HOME/.vibackup"

arg=""

DATE=$(date +%Y-%m-%d-%H:%M:%S)

for arg in $* ; do
  if [ -f "$arg" ] ; then
    bsname=$(basename "$arg")
    cp "$arg" "$viBackup/$bsname$DATE"
  fi
done

exec /usr/bin/vi $*

